﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asm_1
{
    public class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }
        public GiaoVien() { }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }

        public override double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }
        public void NhapThongTin(double heSoLuong)
        {
            HeSoLuong = heSoLuong;
        }

        public void XuLy()
        {
            HeSoLuong += 0.6;
        }

        public new void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}, he so luong: {HeSoLuong}, Luong: {TinhLuong()}");
        }

    }
}
