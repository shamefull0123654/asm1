﻿namespace asm_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Nhap so luong giao vien");
            int n = int.Parse(Console.ReadLine());

            List<GiaoVien> danhsachGiaoVien = new List<GiaoVien>();
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Nhap thong tin cho giao vien {i + 1}:");
                Console.Write("Ho ten: ");
                string hoTen = Console.ReadLine();
                Console.Write("Nam sinh: ");
                int namSinh;
                try
                {
                    Console.Write("Nam sinh: ");
                    namSinh = int.Parse(Console.ReadLine());

                    
                    if (namSinh < 1900 || namSinh > 2100)
                    {
                        throw new Exception("Nam sinh khong hop le.");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Nam sinh khong hop le. Vui long nhap lai.");
                    i--; 
                    continue;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    i--; 
                    continue;
                }

                    

                    Console.Write("Luong co ban: ");
                double luongCoBan = double.Parse(Console.ReadLine());
                Console.Write("He so luong: ");
                double heSoLuong = double.Parse(Console.ReadLine());

                GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
                danhsachGiaoVien.Add(giaoVien);
            }
            GiaoVien giaovienLuongThapNhat = danhsachGiaoVien.OrderBy(gv => gv.TinhLuong()).FirstOrDefault();
            if (giaovienLuongThapNhat != null)
            {
                Console.WriteLine("\nThong tin cua giao vien co luong thap nhat:");
                giaovienLuongThapNhat.XuatThongTin();
            }
            else
            {
                Console.WriteLine("\nKhong co giao vien nao trong danh sach.");
            }

            
            Console.ReadLine();
        }
    }
}